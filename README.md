<h2 align="center">Fullstack MERN Ecommerce - React/Express</h2>
<br>

### Backend Overview:

<p>Built using Express.js and MongoDB, with Mongoose as the ORM. It handles authentication using JWT tokens stored in HTTP-only cookies for enhanced security. The system supports two roles: Regular users (shoppers) can browse products and place orders. Admin users, have additional permissions to manage products and users.</p>

### Architecture:

- **JWT**: The JWT token is stored in an HTTP-only cookie on the server, ensuring it cannot be accessed or manipulated by client-side scripts.
- **Models**:
  - Order - Defines the schema for orders, including details about the user who placed the order, the items in the order, the shipping address, payment information, and the status of the order.
  - Product - Defines the schema for products, including details about the product, its reviews, and its stock information.
  - User - Defines the schema for users, including methods for password hashing and validation.
- **Controllers**:
  - Prouct - Responsible for handling product-related operations including fetching all and single products.
