## React-redux and bootstrap frontend

### Usage

- **npm run dev** to start the frontend.

### Frontend Overview:

- CRUD operations for users after initial login/registration screens.

### Routes:

- /login: Upon launching the frontend will redirect to /login if not signed in. (There is currently no logout functionality).
- /register: Create/Register a new user.

### Application:

- Create/Update/Delete a user. (Create User is the same as Register).

### Dependencies:

- react-bootstrap: UI library.
- react-icons: Displaying svgs.
- react-toastify: For notifications/alerts.

### Architecture:

- screens:

  - HomeScreen: displayed once logged in/registered and used for the main functionality of the application
  - LoginScreen: initial screen for logging in a user
  - RegisterScreen: screen for registering a user(same as creating a user once logged in)

- store.js:
  - redux configuration for initialization of redux and the slices/reducers.
- slices
  - apiSlice: base api slice for creating asynchronous api calls.
  - usersApiSlice: user related actions/methods for triggering async endpoint calls from the apiSlice.
  - authSlice - storing a logged in user to local storage
