import { Navigate, Outlet } from "react-router-dom";
import { useSelector } from "react-redux";

// return Outlet(screen) if logged in otherwise redirect
const PrivateRoute = () => {
  const { userInfo } = useSelector((state) => state.auth);
  return userInfo ? <Outlet /> : <Navigate to="/login" replace />;
};
export default PrivateRoute;
