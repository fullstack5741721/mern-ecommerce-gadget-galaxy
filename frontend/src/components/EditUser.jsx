import { useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { useGetUserDetailsQuery } from "../slices/usersApiSlice";
import Modal from "react-bootstrap/Modal";

function EditUser({
  userId,
  firstName,
  setFirstName,
  lastName,
  setLastName,
  email,
  setEmail,
  dateOfBirth,
  setDateOfBirth,
  file,
  setFile,
  submitHandler,
  show,
  close,
  uploadFileHandler,
}) {
  const { data: userDetails, isLoading: isUserDetailsLoading } =
    useGetUserDetailsQuery(userId); // fetch user details

  useEffect(() => {
    if (userDetails) {
      setFirstName(userDetails.firstName);
      setLastName(userDetails.lastName);
      setEmail(userDetails.email);
      setDateOfBirth(userDetails.dateOfBirth);
    }
  }, [userDetails]);

  return (
    <>
      <Modal show={show} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Update User Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <>
            <Form onSubmit={submitHandler}>
              <Form.Group className="my-2" controlId="firstName">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="firstName"
                  placeholder="Enter first name"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter last name"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="email">
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="dob">
                <Form.Label>Date of birth</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter date of birth"
                  value={dateOfBirth}
                  onChange={(e) => setDateOfBirth(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId="file" className="my-2">
                <Form.Label>File</Form.Label>
                {/* <Form.Control
                  type="text"
                  placeholder="Enter file url"
                  value={file}
                  onChange={(e) => setFile}
                ></Form.Control> */}
                <Form.Control
                  type="file"
                  label="Chose file"
                  onChange={uploadFileHandler}
                ></Form.Control>
              </Form.Group>
              <Button className="w-100" type="submit" variant="primary">
                Update User
              </Button>

              {/* {isLoading && <Loader />} */}
            </Form>
          </>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default EditUser;
