import { Form, Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";

function CreateUser({
  uploadFileHandler,
  firstName,
  setFirstName,
  lastName,
  setLastName,
  email,
  setEmail,
  dateOfBirth,
  setDateOfBirth,
  file,
  setFile,
  password,
  setPassword,
  confirmPassword,
  setConfirmPassword,
  submitHandler,
  show,
  close,
}) {
  return (
    <>
      <Modal show={show} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Create User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <>
            <Form onSubmit={submitHandler}>
              <Form.Group className="my-2" controlId="firstName">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="firstName"
                  placeholder="Enter first name"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter last name"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="email">
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="dob">
                <Form.Label>Date of birth</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter date of birth"
                  value={dateOfBirth}
                  onChange={(e) => setDateOfBirth(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group className="my-2" controlId="confirmPassword">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm password"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                ></Form.Control>
              </Form.Group>

              <Button className="w-100" type="submit" variant="primary">
                Create User
              </Button>

              {/* {isLoading && <Loader />} */}
            </Form>
          </>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default CreateUser;
