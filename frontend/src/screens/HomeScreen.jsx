import { Table, Button, Row, Col } from "react-bootstrap";
import { toast } from "react-toastify";
import { useSelector, useDispatch } from "react-redux";
import { FaEdit, FaTrash } from "react-icons/fa";
import Product from "@/components/Product";
import { useGetProductsQuery } from "@/slices/productsApiSlice";

const HomeScreen = () => {
  // fetches products with loading and error on mount
  const { data: products, isLoading, error } = useGetProductsQuery();
  // useEffect(() => {
  //   const fetchProducts = async () => {
  //     // const { data } = await axios.get("/api/products");
  //     const { data } = await axios.get("http://localhost:8000/api/products");
  //     setProducts(data);
  //   };

  //   fetchProducts();
  // }, []);

  return (
    <>
      {isLoading ? (
        <h2>Loading....</h2>
      ) : error ? (
        <div>{error?.data?.message || error.error}</div>
      ) : (
        <>
          <h1>Latest Products</h1>
          <Row>
            {products.map((product) => (
              <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
                <Product product={product} />
              </Col>
            ))}
          </Row>
        </>
      )}

      {/* <Row className="align-items-center">
        {modalType && modalType.type === "create" && (
          <CreateUser
            firstName={firstName}
            setFirstName={setFirstName}
            lastName={lastName}
            setLastName={setLastName}
            email={email}
            setEmail={setEmail}
            dateOfBirth={dateOfBirth}
            setDateOfBirth={setDateOfBirth}
            file={file}
            setFile={setFile}
            password={password}
            setPassword={setPassword}
            confirmPassword={confirmPassword}
            setConfirmPassword={setConfirmPassword}
            submitHandler={submitNewUser}
            show={modalType.type === "create"}
            close={() => setModalType(null)}
          />
        )}
        {modalType && modalType.type === "edit" && (
          <EditUser
            userId={userId}
            uploadFileHandler={uploadFileHandler}
            firstName={firstName}
            setFirstName={setFirstName}
            lastName={lastName}
            setLastName={setLastName}
            email={email}
            setEmail={setEmail}
            dateOfBirth={dateOfBirth}
            setDateOfBirth={setDateOfBirth}
            file={file}
            setFile={setFile}
            submitHandler={editSubmitUser}
            show={modalType.type === "edit"}
            close={() => setModalType(null)}
          />
        )}
        {modalType && modalType.type === "delete" && (
          <DeleteUser
            email={modalType.email}
            show={modalType.type === "delete"}
            close={() => setModalType(null)}
            handleClose={() => {
              deleteHandler(modalType.id);
              setModalType(null);
            }}
          />
        )}
        <Col>
          <h1>Users</h1>
        </Col>
        <Col className="text-end">
          <Button className="btn-sm m-3" onClick={() => handleModal("create")}>
            <FaEdit /> Create User
          </Button>
        </Col>
      </Row>
      {isUsersLoading ||
      isDeleteLoading ||
      isCreateLoading ||
      isUpdateLoading ? (
        <Loader />
      ) : usersError || deleteError || createError || updateError ? (
        <Message variant="danger">
          {usersError?.error ||
            deleteError?.error ||
            createError?.error ||
            updateError?.error ||
            "Failed to get data"}
        </Message>
      ) : (
        <>
          <Table striped hover responsive className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Date of Birth</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => (
                <tr key={user.id}>
                  <td>{user.id}</td>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                  <td>{user.email}</td>
                  <td>{user.dateOfBirth}</td>
                  <td>
                    <Button
                      disabled={user.email === userInfo.email}
                      onClick={() => {
                        setUserId(user.id);
                        handleModal("edit");
                      }}
                      variant="light"
                      className="btn-sm mx-2"
                    >
                      <FaEdit />
                    </Button>

                    <Button
                      disabled={user.email === userInfo.email}
                      variant="danger"
                      className="btn-sm"
                      onClick={() => {
                        handleModal("delete", user.email, user.id);
                      }}
                    >
                      <FaTrash style={{ color: "white" }} />
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </>
      )} */}
    </>
  );
};

export default HomeScreen;
