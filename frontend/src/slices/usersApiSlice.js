import { apiSlice } from "./apiSlice";

export const userApi = apiSlice.injectEndpoints({
  reducerPath: "userApi",
  // baseQuery: fetchBaseQuery({ baseUrl: BASE_URL }),
  endpoints: (builder) => ({
    getUsers: builder.query({
      query: () => ({
        url: "/users",
      }),
      providesTags: ["User"],
    }),
    getUserDetails: builder.query({
      query: (productId) => ({
        url: `/user/${productId}`,
      }),
      keepUnusedDataFor: 5,
    }),
    deleteUser: builder.mutation({
      query: (userId) => ({
        url: `/user/${userId}`,
        method: "DELETE",
      }),
      providesTags: ["User"],
    }),
    login: builder.mutation({
      query: (data) => ({
        url: "/login",
        method: "POST",
        body: data,
      }),
    }),
    createUser: builder.mutation({
      query: (data) => ({
        url: "/users",
        method: "POST",
        body: data,
      }),
      providesTags: ["User"],
    }),
    updateUserDetails: builder.mutation({
      query: (data) => ({
        url: `user/${data.userId}`,
        method: "PUT",
        body: data,
      }),
      invalidatesTags: ["User"],
    }),
    uploadPdf: builder.mutation({
      query: (data) => ({
        url: "/upload",
        method: "POST",
        body: data,
      }),
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/logout",
        method: "POST",
      }),
    }),
  }),
});

export const {
  useGetUsersQuery,
  useGetUserDetailsQuery,
  useLoginMutation,
  useLogoutMutation,
  useUploadPdfMutation,
  useCreateUserMutation,
  useDeleteUserMutation,
  useUpdateUserDetailsMutation,
} = userApi;
// export default userApi;
