// instead of using async/await and try/catch on all the methods as mongoose methods are asynchronous
const asyncHandler = (fn) => (req, res, next) =>
  Promise.resolve(fn(req, res, next)).catch(next);

export default asyncHandler;
