import mongoose from "mongoose";
import dotenv from "dotenv";
import colors from "colors";
import users from "./data/users.js";
import products from "./data/products.js";
import User from "./models/userModel.js";
import Product from "./models/productModel.js";
import Order from "./models/orderModel.js";
import connectDB from "./config/db.js";

// Load environment variables from .env file
dotenv.config();

// Connect to the MongoDB database
connectDB();

// see db data
const importData = async () => {
  // Clear existing data from Order, Product, and User collections
  try {
    await Order.deleteMany();
    await Product.deleteMany();
    await User.deleteMany();

    // Insert sample user data into the User collection
    const createdUsers = await User.insertMany(users);

    // Get the ID of the first user(admin)
    const adminUser = createdUsers[0]._id;

    // Assign the adminUser ID to each product in the sample data
    const sampleProducts = products.map((product) => {
      return { ...product, user: adminUser };
    });

    // insert products sample data
    await Product.insertMany(sampleProducts);

    console.log("Data Imported!".green.inverse);
    process.exit();
  } catch (error) {
    console.error(`${error}`.red.inverse);
    process.exit(1);
  }
};

/// destroy all data from the database
const destroyData = async () => {
  try {
    // Clear existing data from Order, Product, and User collections
    await Order.deleteMany();
    await Product.deleteMany();
    await User.deleteMany();

    console.log("Data Destroyed!".red.inverse);
    process.exit();
  } catch (error) {
    console.error(`${error}`.red.inverse);
    process.exit(1);
  }
};

// ability to choose which task to run based on cli command (check for -d argument):
// npm run data:import/npm run data:destroy
if (process.argv[2] === "-d") {
  destroyData();
} else {
  importData();
}
