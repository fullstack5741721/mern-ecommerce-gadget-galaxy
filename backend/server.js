import express from "express";
import dotenv from "dotenv";
dotenv.config();
import connectDb from "./config/db.js"; // mongodb connection
import { notFound, errorHandler } from "./middleware/errorMiddleware.js"; // error middleware
import productRoutes from "./routes/productRoutes.js";
const port = process.env.PORT || 5000;

connectDb(); // Connect to MongoDB
const app = express();

app.get("/", (req, res) => {
  res.send("API is running");
});

app.use("/api/products", productRoutes); // /products, /product/id
app.use(notFound);
app.use(errorHandler);
app.listen(port, () => console.log(`Server running on port ${port}`));
