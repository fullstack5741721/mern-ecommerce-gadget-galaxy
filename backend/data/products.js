// seeder data
const products = [
  {
    name: "Apple AirPods Pro (2nd Generation)",
    image: "/images/airpods.jpeg",
    description:
      "The AirPods Pro (2nd Generation) offers active noise cancellation, transparency mode, and a customizable fit for all-day comfort.",
    brand: "Apple",
    category: "Electronics",
    price: 249.99,
    countInStock: 15,
    rating: 4.8,
    numReviews: 45,
  },
  {
    name: "iPhone 14 Pro Max 512GB",
    image: "/images/iphone.jpeg",
    description:
      "The iPhone 14 Pro Max features a 6.7-inch Super Retina XDR display, ProMotion technology, and a 48MP main camera for stunning photography.",
    brand: "Apple",
    category: "Electronics",
    price: 1399.99,
    countInStock: 10,
    rating: 4.9,
    numReviews: 60,
  },
  {
    name: "Canon EOS R5 Mirrorless Camera",
    image: "/images/cannon.jpeg",
    description:
      "The Canon EOS R5 offers a 45MP full-frame sensor, 8K video recording, and advanced autofocus capabilities, making it perfect for professional photographers.",
    brand: "Canon",
    category: "Electronics",
    price: 3899.99,
    countInStock: 8,
    rating: 4.7,
    numReviews: 25,
  },
  {
    name: "Sony PlayStation 5",
    image: "/images/ps5.jpg",
    description:
      "Experience lightning-fast loading with an ultra-high-speed SSD, deeper immersion with support for haptic feedback, adaptive triggers, and 3D Audio, and an all-new generation of incredible PlayStation games.",
    brand: "Sony",
    category: "Electronics",
    price: 499.99,
    countInStock: 20,
    rating: 4.9,
    numReviews: 75,
  },
  {
    name: "Logitech G Pro X Superlight Wireless Gaming Mouse",
    image: "/images/logitech-mouse.jpg",
    description:
      "The Logitech G Pro X Superlight is ultra-lightweight at under 63 grams and features advanced HERO 25K sensor for unparalleled precision.",
    brand: "Logitech",
    category: "Electronics",
    price: 149.99,
    countInStock: 12,
    rating: 4.6,
    numReviews: 40,
  },
  {
    name: "Amazon Echo Dot (5th Generation)",
    image: "/images/echo.jpeg",
    description:
      "The Amazon Echo Dot (5th Generation) features improved sound quality, a temperature sensor, and eero built-in for better Wi-Fi coverage.",
    brand: "Amazon",
    category: "Electronics",
    price: 59.99,
    countInStock: 25,
    rating: 4.5,
    numReviews: 50,
  },
  {
    name: "Samsung Galaxy S23 Ultra",
    image: "/images/galaxy-ultra.jpeg",
    description:
      "The Samsung Galaxy S23 Ultra features a 200MP camera, 8K video recording, and a 5000mAh battery for all-day power.",
    brand: "Samsung",
    category: "Electronics",
    price: 1199.99,
    countInStock: 15,
    rating: 4.8,
    numReviews: 30,
  },
  {
    name: "Apple MacBook Pro 14-inch (M2 Pro, 2023)",
    image: "/images/macbook-pro.jpeg",
    description:
      "The Apple MacBook Pro 14-inch with the M2 Pro chip delivers exceptional performance, a stunning Liquid Retina XDR display, and up to 18 hours of battery life.",
    brand: "Apple",
    category: "Electronics",
    price: 2499.99,
    countInStock: 5,
    rating: 4.7,
    numReviews: 22,
  },
  {
    name: "Sony WH-1000XM5 Wireless Headphones",
    image: "/images/sony.jpeg",
    description:
      "The Sony WH-1000XM5 headphones feature industry-leading noise cancellation, up to 30 hours of battery life, and superior sound quality.",
    brand: "Sony",
    category: "Electronics",
    price: 399.99,
    countInStock: 10,
    rating: 4.9,
    numReviews: 35,
  },
  {
    name: "Microsoft Surface Pro 9",
    image: "/images/surface-pro.jpg",
    description:
      "The Microsoft Surface Pro 9 offers the flexibility of a tablet and the performance of a laptop, with a high-resolution 13-inch touchscreen and the latest Intel processors.",
    brand: "Microsoft",
    category: "Electronics",
    price: 1099.99,
    countInStock: 10,
    rating: 4.6,
    numReviews: 15,
  },
  {
    name: "Google Nest Hub Max",
    image: "/images/google-nest.jpeg",
    description:
      "The Google Nest Hub Max provides a 10-inch HD screen, hands-free help with Google Assistant, and enhanced audio quality for a richer sound experience.",
    brand: "Google",
    category: "Electronics",
    price: 229.99,
    countInStock: 12,
    rating: 4.3,
    numReviews: 28,
  },
  {
    name: "DJI Air 2S Drone",
    image: "/images/drone.jpeg",
    description:
      "The DJI Air 2S offers a 1-inch CMOS sensor, 5.4K video recording, and up to 31 minutes of flight time with intelligent shooting modes.",
    brand: "DJI",
    category: "Electronics",
    price: 999.99,
    countInStock: 7,
    rating: 4.7,
    numReviews: 33,
  },
];

export default products;
